FROM ubuntu:20.04

RUN apt-get update && apt-get install -y --no-install-recommends

RUN apt-get update && apt-get install -y --no-install-recommends \
        build-essential \
        curl \
        python \
        python-dev \
        python3-pip \
        python-setuptools \
        wget \
        unzip \
        tcpdump \
        iputils-ping \
        net-tools \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*


RUN python3 -m pip install scapy
